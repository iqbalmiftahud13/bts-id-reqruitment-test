declare namespace API {
   interface ResponseDataBody<T>{
        statusCode: number
        message: string
        errorMessage: string
        data: T
    
   }
}