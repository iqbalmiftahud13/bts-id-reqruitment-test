declare namespace Model {
    module User {
        interface Data {
            email: string
            password : string
            name: string,
            role: 'admin' | 'user'
        }

        interface LoginData {
            token: string
        }
    }
}