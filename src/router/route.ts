import { createRouter, createWebHistory, RouteRecordRaw  } from 'vue-router'
import {useUserStore} from "@/store/userStore"
const routes:Array<RouteRecordRaw>  = [
    {
        path: '/',
        meta: { title: 'Home', layout: 'blank', isPublic : true },
        component: () => import('@/views/Login.vue'),
    },
    {
        path: '/register',
        meta: { title: 'Register', layout: 'blank', isPublic : true },
        component: () => import('@/views/Register.vue'),
    },
    {
        path: '/admin/dashboard',
        meta: { title: 'Dashboard', layout: 'dashboard', isPublic : false },
        component: () => import('@/views/dashboard/index.vue'),
    },
    {
        path: '/admin/checklist',
        meta: { title: 'Check List Item', layout: 'dashboard', isPublic : false },
        component: () => import('@/views/dashboard/checklist/index.vue'),
    },
]
const router = createRouter({
    history: createWebHistory(),
    routes,
    linkExactActiveClass: 'text-yellow',
})



router.beforeEach((to, from, next) => {
  const store = useUserStore()
  const loggedIn = store.getUser ? true : false

  if (to.matched.some(record => !record.meta.isPublic)) {
    if (!loggedIn) {
      next('/login')
    }
  }
  next()
})

export default router
