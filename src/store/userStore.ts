import { defineStore } from "pinia"
import { login } from "@/helper/API/user"
import router from '@/router/route'

export const useUserStore = defineStore("userStore",{
  state: (): { user: Model.User.LoginData | null } => ({
    user: {
        token: ''
    },
  }),
  persist: true,

  getters: {
    isAuthenticated(state) {
      return state.user !== null
    },

    getUser(state) {
      return state.user
    },
  },

  actions: {
    async login(payload :any) {
        await login(payload).then((resp: Model.User.LoginData)=>{
            localStorage.setItem('user_session', JSON.stringify(resp));
            localStorage.setItem('access_token', resp.token);
            this.user = resp
            router.push('/admin/dashboard')
        })
        .catch((error: any)=>{
            console.log(error);
            throw error
        })
    },

    logout() {
      this.$reset()
      localStorage.clear()
      router.push('/')
    },
  },
})
