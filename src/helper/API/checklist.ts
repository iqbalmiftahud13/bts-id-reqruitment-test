import axios from "@/utils/service";

const CHECKLIST_ENDPOINT = `/checklist`

export async function getCheckList():Promise<any> {
    const { data } = await axios.get<API.ResponseDataBody<any>>(`${CHECKLIST_ENDPOINT}`)
    return data
}

export async function postCheckList(payload:any):Promise<any> {
    const { data } = await axios.post<API.ResponseDataBody<any>>(`${CHECKLIST_ENDPOINT}`, {...payload})
    return data.data
}

export async function deleteCheckList(id: any):Promise<any> {
    const { data } = await axios.delete<API.ResponseDataBody<any>>(`${CHECKLIST_ENDPOINT}/${id}`)
    return data
}
