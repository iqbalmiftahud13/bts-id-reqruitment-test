import axios from "@/utils/service";

const REGIST_ENDPOINT = `/register`
const LOGIN_ENDPOINT = `/login`

export async function register(payload:any):Promise<any> {
    const { data } = await axios.post<API.ResponseDataBody<any>>(`${REGIST_ENDPOINT}`, {...payload})
    return data.data
}

export async function login(payload:any):Promise<Model.User.LoginData>{
    const { data } = await axios.post<API.ResponseDataBody<Model.User.LoginData>>(`${LOGIN_ENDPOINT}`, {...payload})
    return data.data
}